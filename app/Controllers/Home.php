<?php

namespace App\Controllers;

use Config\Services;
use App\Models\FamiliaModel;
use App\Models\ProductoModel;
use App\Models\LineaModel;


class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index()
    {
        if (!$this->auth->loggedIn()){
            return redirect()->to('auth/login');
        }
        return view('welcome_message');
    }
    
    public function catalogo() {
        $familiaModel = new FamiliaModel();
        $lineaModel = new LineaModel();
        
        $data['familias'] = $familiaModel
                ->distinct()
                ->select ('familias.CodigoFamilia, familias.NombreFamilia, count(productos.CodigoProducto) as cuenta')
                ->join('productos','familias.CodigoFamilia=productos.CodigoFamilia','LEFT')
                ->groupBy('familias.CodigoFamilia')
                ->findAll();
        
        $data['lineas'] = $lineaModel
                ->select ('lineas.CodigoLinea, lineas.NombreLinea, lineas.CodigoFamilia')
                ->join('familias','lineas.CodigoFamilia=familias.CodigoFamilia','LEFT')
                ->findAll();

        $data['titol'] = 'Catalogo Molero';
        
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        
        return view('articulos/catalogo', $data);
    }
    
    public function productos() {
        $productoModel = new ProductoModel();
        
        $data['productos'] = $productoModel
                ->select ("productos.CodigoProducto, productos.Nombre, productos.Talla, concat(Nombre, ' TALLA ',Talla) as concatenado")
                ->join('familias','productos.CodigoFamilia=familias.CodigoFamilia','LEFT')
                ->findAll();
        
        $data['titol'] = 'Productos Molero';
        
        return view('articulos/productos', $data);
    }
    
    public function borrar($CodigoProducto){
        if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
            $productoModel = new ProductoModel();
            
            $productoModel->delete($CodigoProducto);
            return redirect()->to('/home/Productos');
        }else {
            
            return view('articulos/noPuedes');
        }
}
}
