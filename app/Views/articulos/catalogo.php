<?= $this->extend('layout/template') ?>

<?= $this->section('content') ?>
<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <div class="d-flex flex-row" style="margin-right: 300px;">
                        <?php if ($auth->loggedIn()): ?>
                            <?php $user = $auth->user()->row(); ?>
                            <li class="nav-item">
                                <button class="btn btn-outline-success">Usuario: <?= $user->first_name . ' ' . $user->last_name ?></button>
                            </li>&nbsp;&nbsp;&nbsp;
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('index.php/auth/logout') ?>">Salir</a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('index.php/auth/login') ?>">Entrar</a>
                            </li>
                        <?php endif; ?>
                            <li class="nav-item">
                                <a href="<?= base_url('index.php/home/productos') ?>" class="nav-link">Ver productos</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('index.php/home/catalogo') ?>" class="nav-link">Ver catalogo</a>
                            </li>
                        </div>  
                    </ul>
                </div>
            </div>
        </nav><br>

<?php foreach ($familias as $familia): ?>
<div class="d-grid gap-2 d-md-block">

    <a><button class="btn btn-outline-dark"><img src= "<?=base_url('assets/fotos/iconostienda/' . sprintf('%010s', $familia['CodigoFamilia'])). '_01' ?>.png" width="50px">
                                            <?=$familia['NombreFamilia']?><br><i class="bi bi-book">&nbsp;<?=$familia['cuenta']?>&nbsp;productos</i></button></a>

</div>
<br>
<?php endforeach; ?>

<?= $this->endSection() ?>