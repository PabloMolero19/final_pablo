<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductoModel extends Model{
    protected $table = 'productos';
    protected $allowedFields = ['CodigoProducto', 'Nombre', 'CodigoFamilia', 'CodigoLinea', 'ReferenciaProducto', 'Caracteristicas',
                               'Indicaciones', 'Efectos', 'Material', 'altura', 'lado', 'Talla', 'Color', 'PesoBrutoUnitario',
                               'PesoNetoUnitario', 'TipoIVA'];
    protected $returnType = 'array';
    protected $primaryKey = 'CodigoProducto';
}

