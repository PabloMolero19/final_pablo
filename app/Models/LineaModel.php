<?php

namespace App\Models;

use CodeIgniter\Model;

class LineaModel extends Model{
    protected $table = 'lineas';
    protected $allowedFields = ['CodigoLinea', 'NombreLinea', 'CodigoFamilia'];
    protected $returnType = 'array';
}
