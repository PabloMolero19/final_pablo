<?php
namespace App\Models;

use CodeIgniter\Model;

class FamiliaModel extends Model{
    protected $table = 'familias';
    protected $allowedFields = ['CodigoFamilia', 'NombreFamilia'];
    protected $returnType = 'array';
}